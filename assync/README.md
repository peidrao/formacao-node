## Programação Assíncrona
### Três formas de trabalhar com programação Assíncrona
1. **Callback**
2. **Promises**  (evolução do callback)
  - Muito usado em then e catch
3. **Async/Await** foi criado a partir do ES7