function getUser() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve([
        { name: 'Pedro', lang: 'JS' },
        { name: 'Victor', lang: 'TS' },
        { name: 'Fonseca', lang: 'Python' },
      ]);
    }, 3000);
  });
}

/* 
getUser().then((user) => {
  console.log(user)
})
 */

async function menu() {
  let usuários = await getUser();
  console.log(usuários);
}
menu();
