# Transformar arquivos de Excel em HTML & PDF

## Ideia

O interesse da produção dessa aplicação vem para uma futura inspiração no trabalho com manipulação de grandes bases de dados.
Trazer uma grande quantidade de dados de tabelas, para um formato talvez mais acessível como **PDF**. Ou utilizar esses dados em um formato que tanto gosto, o JSON. Muitas vezes é preciso ter dados em json para uma determinada api consumir seus dados. Existem várias formas de fazer isso? Sim existem aos montes, mas essa aplicação foi feita para aplicar meus conhecimentos na linguagem, dessa forma é meu _Frankenstein_.

## Projeto

Apesar de ser uma aplicação mínima, procurei aplicar o conceito de orientação a objetos. Organização é um princípio que sempre devemos levar para nossas vidas.

## Organização

### Reader.js

Classe responsável pela leitura do arquivo.

### Processor.js

Classe que faz a separação das linhas do arquivos, nesse caso, a separação acontece por linhas e vírgulas (padrão de separação de arquivos **csv**)

## Table.js

Abstração que é feita com o campos da tabelas, cabeçalho (variáveis da tabela) e as linhas que correspondem aos atributos das variáveis.
