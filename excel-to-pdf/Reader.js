const fileSystem = require('fs');
const util = require('util');

class Reader {
  constructor() {
    this.reader = util.promisify(fileSystem.readFile);
  }

  async Read(filePath) {
    try {
      return await this.reader(filePath, 'utf-8');
    } catch (err) {
      return undefined;
    }
  }
}

module.exports = Reader;
