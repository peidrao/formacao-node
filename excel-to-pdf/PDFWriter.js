const Pdf = require('html-pdf');

class PDFWriter {
  static WritePDF(filename, html) {
    Pdf.create(html, {}).toFile(filename, (err) => {});
  }
}

module.exports = PDFWriter;
