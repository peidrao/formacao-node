const fileSystem = require('fs');
const util = require('util');

class Writer {
  constructor() {
    this.writer = util.promisify(fileSystem.writeFile);
  }

  async Write(filename, data) {
    try {
      return await this.writer(filename, data);
    } catch (err) {
      return undefined;
    }
  }
}

module.exports = Writer;
