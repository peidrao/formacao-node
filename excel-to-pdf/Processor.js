class Processor {
  static Process(data) {
    let row = data.split('\r\n');
    let rows = [];

    row.forEach((element) => {
      let array = element.split(',');
      rows.push(array);
    });
    return rows;
  }
}

module.exports = Processor;
