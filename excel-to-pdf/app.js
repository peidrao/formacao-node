const fs = require('fs');
const Reader = require('./Reader');
const Processor = require('./Processor');
const Table = require('./Table');
const HTMLParser = require('./HTMLParser');
const Writer = require('./Writer');
const PDFWriter = require('./PDFWriter');

let read = new Reader();
let write = new Writer();

async function main() {
  let data = await read.Read('./users.csv');
  let dataProcess = Processor.Process(data);

  let users = new Table(dataProcess);

  let html = await HTMLParser.Parse(users);
  write.Write(Date.now() + '.html', html);
  PDFWriter.WritePDF(Date.now() + '.pdf', html)

}

main();
