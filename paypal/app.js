import express from 'express';
import bodyParser from 'body-parser';
import paypal from 'paypal-rest-sdk';
const app = express();

// View Engine
app.set('view engine', 'ejs');

// Body Parser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

paypal.configure({
  mode: 'sandbox',
  client_id:
    'Af0hqsJqFef9PnsYn_tCFhbBk7Z8zbWWZF8lzgCtym3-ML86R0Qbrby9IyG4C5d-1CHZFgqyWt1urrqz',
  client_secret:
    'EJ1mY6zTKmPns5tK16CpG3H6jFvnaHwZWSz3BnkFM7hA_UOi-hCoGpIrBbqHQvD4ljzwDJR9WT6O61Y3',
});

app.get('/', (req, res) => {
  res.render('index');
});

app.post('/comprar', (req, res) => {
  const { name, price, amount, email, id } = req.body;
  let total = amount * price;

  let pagamento = {
    intent: 'sale',
    payer: {
      payment_method: 'paypal',
    },
    redirect_urls: {
      return_url: `http://localhost:9000/final?email=${email}&id=${id}&total=${total}`,
      cancel_url: 'http://cancel.url',
    },
    transactions: [
      {
        item_list: {
          items: [
            {
              name: name,
              sku: name,
              price: price,
              currency: 'BRL',
              quantity: amount,
            },
          ],
        },
        amount: {
          currency: 'BRL',
          total: total,
        },
        description: 'This is the payment description.',
      },
    ],
  };

  paypal.payment.create(pagamento, (error, payment) => {
    if (error) {
      console.log(`erro: ${error}`);
    } else {
      for (let i = 0; i < payment.links.length; i++) {
        let p = payment.links[i];
        if (p.rel === 'approval_url') {
          res.redirect(p.href);
        }
      }
    }
  });
});

app.get('/final', (req, res) => {
  let payerId = req.query.PayerID;
  let paymentId = req.query.paymentId;
  let total = req.query.total;

  let emailCliente = req.query.email;
  let idCliente = req.query.id;
  console.log(emailCliente, idCliente);

  let final = {
    payer_id: payerId,
    transactions: [
      {
        amount: {
          currency: 'BRL',
          total: total,
        },
      },
    ],
  };

  paypal.payment.execute(paymentId, final, (error, payment) => {
    if (error) {
      console.log(error);
    } else {
      res.json(payment);
    }
  });
});

/* Rota para os planos */
app.get('/create', (req, res) => {
  const plan = {
    name: 'Plano Prata',
    description: 'Um plano que vale muito a pena',
    merchant_preferences: {
      auto_bill_amount: 'yes',
      cancel_url: 'http://www.cancel.com',
      initial_fail_amount_action: 'continue',
      max_fail_attempts: '1',
      return_url: 'http://www.success.com',
      setup_fee: {
        currency: 'BRL',
        value: '0',
      },
    },
    payment_definitions: [
      // Plano trial
      {
        amount: {
          currency: 'BRL',
          value: '0',
        },
        cycles: '7',
        frequency: 'DAY',
        frequency_interval: '1',
        name: 'Teste grátis',
        type: 'TRIAL',
      },
      // Plano Regular - Prata
      {
        amount: {
          currency: 'BRL',
          value: '19.90',
        },
        cycles: '0',
        frequency: 'MONTH',
        frequency_interval: '1',
        name: 'Regular Prata',
        type: 'REGULAR',
      },
    ],
    type: 'INFINITE',
  };

  paypal.billingPlan.create(plan, (err, plan) => {
    if (err) {
      console.log(err);
    } else {
      res.json(plan);
    }
  });
});

/* Listar os planos */
app.get('/list', (req, res) => {
  paypal.billingPlan.list({ status: 'ACTIVE' }, (err, plans) => {
    if (err) {
      console.log(err);
    } else {
      res.json(plans);
    }
  });
});

/* Editar status do planos */
app.get('/active/:id', (req, res) => {
  let alteracoes = [
    {
      op: 'replace',
      path: '/',
      value: {
        state: 'ACTIVE',
      },
    },
  ];
  paypal.billingPlan.update(req.params.id, alteracoes, (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send('Alterações feitas');
    }
  });
});

const port = 9000;
app.listen(port, () => {
  console.log(`App rodando na porta: ${port}`);
});
