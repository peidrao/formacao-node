const Pedro = {
  nome: 'Pedro Victor',
  idade: 21,
};

const Andre = {
  nome: 'José André',
  idade: 20,
};

const Rodolfo = {
  nome: 'Rodolfo Dantas',
  idade: 19,
};

const vetor = [Pedro, Andre, Rodolfo];

const Pessoa = vetor.find((user) => user.nome === 'Pedro Victor');

console.log(Pessoa);
