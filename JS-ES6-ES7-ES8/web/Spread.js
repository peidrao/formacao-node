let nome = 'Pedro Victor da Fonseca';
let idade = 21;
let trabalha = false;
let namora = false;

let Pessoa = {
  nome,
  idade,
  trabalha,
  namora,
};

let Jesus = {
  mae: 'Maria Imaculada',
  pai: 'José Castíssimo',
  idade: 33,
};

let Objeto = {
  ...Jesus,
  ...Pessoa,
};

console.log(Objeto);
