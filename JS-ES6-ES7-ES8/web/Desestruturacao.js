let Pessoa = {
  nome: 'Pedro Victor da Fonseca',
  idade: 21,
  curso: 'Tecnologia da Informação',
  nascimento: '25/06/1998',
};

let { nome, idade, curso, nascimento } = Pessoa;

console.log(
  `Olá meu nome é ${nome}, tenho ${idade} anos.\nNasci em ${nascimento} e atualmente faço ${curso}!`
);
