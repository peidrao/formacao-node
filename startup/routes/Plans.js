import express from 'express';
const router = express.Router();

import PlansController from '../controllers/PlansController';

router.get('/admin/plans', PlansController.index);

router.get('/admin/plans/create', PlansController.create);

router.get('/admin/plans/edit/:id', PlansController.edit);

router.get('/plans/deactivate/:id', PlansController.deactivate);

router.post('/plans/store', PlansController.store);

router.post('/plans/update', PlansController.update);

export default router;
