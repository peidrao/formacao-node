'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Plans', 'deactivated', {
      type: Sequelize.DataTypes.BOOLEAN,
    });
  },

  down: (queryInterface, Sequelize) => {},
};
