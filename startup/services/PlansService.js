import database from '../models/index';
class PlansService {
  constructor() {
    this.Plan = database['Plan'];
  }

  async getAll() {
    try {
      return await this.Plan.findAll();
    } catch (error) {
      console.log(error);
      return undefined;
    }
  }

  async getById(id) {
    try {
      return await this.Plan.findByPk(id);
    } catch (error) {
      console.log(error);
      return undefined;
    }
  }

  async update(id, data) {
    let errors = {};
    let isValid = this.validate(data, errors);

    if (isValid) {
      try {
        let plan = await this.getById(id);

        plan.title = data.title;
        plan.client = data.client;
        plan.value = data.value;
        plan.list = data.list;

        await plan.save();
        return true;
      } catch (error) {
        errors.system_msg = 'Não foi possível editar o plano';
        return errors;
      }
    } else {
      return errors;
    }
  }

  async store(plans) {
    let errors = {};

    if (plans.import !== undefined) {
      plans.import = true;
    } else {
      plans.import = false;
    }

    let isValid = this.validate(plans, errors);

    if (isValid) {
      try {
        await this.Plan.create(plans);
        return true;
      } catch (err) {
        console.log(err);
        errors.system_msg = 'Não foi possível salvar o plano';
        return errors;
      }
    } else {
      return errors;
    }
  }

  validate(plan, errors) {
    let erroCount = 0;
    if (plan.title === undefined) {
      errors.title_msg = 'O título é inválido!';
      erroCount++;
    } else {
      if (plan.title.length < 3) {
        errors.title_msg = 'O título é pequeno demais';
        erroCount++;
      }
    }

    if (plan.list === undefined) {
      errors.list_msg = 'A quantidade de listas é inválida';
      erroCount++;
    } else {
      if (plan.list < 1) {
        errors.list_msg = 'A quantidade de listas é insuficiente';
        erroCount++;
      }
    }

    if (erroCount == 0) {
      return true;
    } else {
      return false;
    }
  }

  async deactivate(id) {
    try {
      let plan = await this.getById(id);

      plan.deactivated = true;

      await plan.save();
      return true;
    } catch (error) {
      return false;
    }
  }
}

export default new PlansService();
