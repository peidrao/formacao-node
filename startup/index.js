import express from 'express';
const app = express();
import bodyParser from 'body-parser';
import session from 'express-session';
import flash from 'connect-flash';

import PlansRouter from './routes/Plans';

// View engine
app.set('view engine', 'ejs');

app.use(
  session({
    secret: 'qualquercoisa',
    cookie: { maxAge: 30000000 },
    saveUninitialized: true,
    resave: true,
  })
);

app.use(flash());

app.use(express.static('public'));

//Body parser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/', PlansRouter);

// Router

app.get('/', (req, res) => {
  res.render('index.ejs');
});

// End Router
app.listen(3000, () => {
  console.log('O servidor está rodando!');
});
