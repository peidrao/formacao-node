const { Sequelize } = require('sequelize');

const connection = new Sequelize(
  'perguntasCatolicas',
  'root',
  'tecnologiamariadb',
  {
    host: 'localhost',
    dialect: 'mysql',
    logging: false
  }
);

module.exports = connection;
