const express = require('express');
const bodyParser = require('body-parser');
const connection = require('./database/database');
const Pergunta = require('./database/models/pergunta');
const Resposta = require('./database/models/resposta');

const app = express();
/*  Banco de dados */
connection
  .authenticate()
  .then(() => {
    console.log('Conexão feita com sucesso');
  })
  .catch(err => {
    console.log(err);
  });

app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/', (req, res) => {
  Pergunta.findAll({ raw: true, order: [['id', 'desc']] }).then(perguntas => {
    res.render('index', {
      perguntas: perguntas
    });
  });
});

app.get('/perguntar', (req, res) => {
  res.render('perguntar/perguntar');
});

app.post('/salvarpergunta', (req, res) => {
  const { titulo, descricao } = req.body;
  Pergunta.create({
    titulo: titulo,
    descricao: descricao
  })
    .then(() => {
      res.redirect('/');
    })
    .catch(err => {
      console.log(`Erro: ${err}`);
    });
});

app.get('/pergunta/:id', (req, res) => {
  const id = req.params.id;
  Pergunta.findOne({
    where: { id: id }
  }).then(pergunta => {
    if (pergunta !== undefined) {
      Resposta.findAll({
        where: { perguntaId: pergunta.id },
        order: [['id', 'desc']]
      }).then(respostas => {
        res.render('perguntar/pergunta', {
          pergunta: pergunta,
          respostas: respostas
        });
      });
    } else {
      res.redirect('/');
    }
  });
});

app.post('/responder', (req, res) => {
  const corpo = req.body.corpo;
  const perguntaId = req.body.pergunta;
  console.log(`Corpo: ${corpo}\nPergunta: ${perguntaId}`);
  Resposta.create({
    corpo: corpo,
    perguntaId: perguntaId
  })
    .then(() => {
      res.redirect(`/pergunta/${perguntaId}`);
      console.log(perguntaId);
    })
    .catch(err => {
      console.log(`erro: ${err}`);
    });
});

const port = 8080;
app.listen(port, () => {
  console.log(`Servidor rodando na porta: ${port}`);
});
