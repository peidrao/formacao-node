# Plataforma de perguntas e respostas

## Tecnologias usadas
* Express
* mySQL
* EJS (Template Enginer)
  * Desenhar o html

### Configurando o EJS c/ express
Antes de colocar as rotas, basta setar o projeto com uma view engine
```  app.set('view engine', 'ejs'); ``` 

### EJS
``` <% =variavelemjs %> ``` 
Exibir variáveis de JavaScript no EJS

#### Estrutura condicional
```
const valor = true;
<% if(valor) { %>
  <h1> Olá </h1>
<% } else { %>
  <h1> Olá, não! </h1>
<% } %>
 ``` 
 
#### Estrutura de repetição

> **Arquivo.js**
```
const nomes = [
  {nome: "Pedro"},
  {nome: "João"},
  {nome: "Matheus"},
  {nome: "Lucas"},
]
```

> **Arquivo.ejs**
```
<% nomes.forEach(produto =>{  %>
  <h1>
    <%=produto.nome %>
  </h1>
<% }) %>
```

### Incluir Partials EJS
``` <%- include ('./partials/arquivo'); %> ```

## Body Parser
Serve para incluir dados de formulário em seu **back-end**

```
app.use(bodyParser.urlencoded({ extended: false }));
```
Ao usuário enviar os dados o bodyparser vai construir uma estrutura JavaScript. **Decodifica os dados do usuário**

## Sequelize
> yarn add sequelize

### Conectar mySQL ao Sequelize
* Criar um módulo para o banco de dados:  _mkdir database && touch database.js_
* Importar o módulo do Sequelize no database.js: _const Sequelize = require('sequelize');_

### Conexão Banco de dados -> back-end
**database.js**
```
const { Sequelize } = require('sequelize');
const connection = new Sequelize(
  'perguntasCatolicas',
  'root',
  'tecnologiamariadb',
  {
    host: 'localhost',
    dialect: 'mysql'
  }
);
module.exports = connection;
 ```

**index.js**
```
const connection = require('./database/database.js')
connection.authenticate().then(() => console.log("Conexão deu certo")).catch((err) => {console.log(err)})
```

## Gerando tabelas com models
* Criar um model para o banco de dados:  _mkdir model && touch pergunta.js_
* Colocar configurações do model de _pergunta.js_

## Salvando dados de formulário em banco de dados
Exportando o seu model de pergunta, para o arquivo de rotas, é necessaŕio dentro da rota de salvar a pergunta no banco de dados criar o método
```
Pergunta.create({ titulo: titulo, descricao: descricao})
```