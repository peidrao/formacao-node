const express = require('express'); // Importando express

const app = express(); // Iniciando express

/* Criando a primeira rota */
app.get('/', (req, res) => {
  res.json({ mensagem: 'Olá meus amigos' });
});

/* Porta da aplicação */
const PORT = 8080;
app.listen(PORT, () => {
  console.log(`Servidor rodando na porta ${PORT}`);
});
