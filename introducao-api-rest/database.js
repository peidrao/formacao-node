const database = {
  games: [
    {
      id: 23,
      title: 'Call of Duty',
      year: 2019,
      price: 60,
    },
    {
      id: 44,
      title: 'PES2020',
      year: 2020,
      price: 80,
    },
    {
      id: 2,
      title: 'Minecraft',
      year: 2012,
      price: 20,
    },
  ],
};

export default database;
