import express from 'express';
import bodyParser from 'body-parser';
const app = express();
import database from './database';

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/games', (req, res) => {
  res.statusCode = 200;
  res.json(database.games);
});

app.get('/game/:id', (req, res) => {
  if (isNaN(req.params.id)) {
    res.sendStatus(400);
  } else {
    let id = parseInt(req.params.id);
    let game = database.games.find((i) => i.id === id);
    if (game != undefined) {
      res.statusCode = 200;
      res.json(game);
    } else {
      res.sendStatus(404);
    }
  }
});

app.post('/game', (req, res) => {
  const { id, title, price, year } = req.body;

  database.games.push({
    id,
    title,
    price,
    year,
  });

  res.sendStatus(200);
});

app.delete('/game/:id', (req, res) => {
  if (isNaN(req.params.id)) {
    res.sendStatus(400);
  } else {
    let id = parseInt(req.params.id);
    let index = database.games.findIndex((g) => g.id === id);

    if (index === -1) {
      res.sendStatus(404);
    } else {
      database.games.splice(index, 1);
      res.sendStatus(200);
    }
  }
});

app.put('/game/:id', async (req, res) => {
  if (isNaN(req.params.id)) {
    res.sendStatus(400);
  } else {
    let id = parseInt(req.params.id);
    let game = await database.games.find((i) => i.id === id);
    if (game != undefined) {
      let { title, price, year } = req.body;
      if (title !== undefined) {
        game.title = title;
      }
      if (price !== undefined) {
        game.price = price;
      }
      if (year !== undefined) {
        game.year = year;
      }
    } else {
      res.sendStatus(404);
    }
  }
});

app.listen(8000, () => {
  console.log('Api rodando');
});
