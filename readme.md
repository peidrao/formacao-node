# Node.JS :computer:

Interpretador javascript, que roda fora dos navegadores

## Por que usar **NODE.JS**

- Muito leve
- Muito rápido
- Usa JavaScript
- Tem um dos maiores ecossistemas do mundo.
- Está sendo utilizado fortente no mercado.

## Protocolo HTTP

O HTTP é dividido em duas ações, **requisição** e **resposta**

```
const http = require('http');
http.createServer((req, res) => {
    res.end('<h1> Olá </h1>');
  }).listen(8181);
```

## Express

> npm install express --save

## Parâmetros

### Params

Pega os valores que estão nos parâmetros da url.
```
app.get('/empresa/:id', (req, res) => {
  let idEmpresa = req.params.id;
  res.send(`O id da empresa é: ${idEmpresa}`);
})
```

### Query Params 

Passar os parâmetros de forma amigável (Não são tão seguros)

```
app.get('/canal/youtube', (req, res) => {
  let canal = req.query["canal"];
  if(canal) {
    res.send(canal);
  } else {
    res.send("Nenhum canal foi selecionado");
  }
})
```

## Mysql

> mysql -h localhost -u root -p

**Mysql:** Banco de dados
**-h:** Qual servidor o banco será conectado
**localhost:** Servidor local (meu computador)
**-u:** Qual usuário será conectado
**root:** Super usuário
**-p:** Requer digitar a senha

### Comandos do MYSQL

Mostrar tabela
> show databases;

Criar um banco de dado
> create database nomedoBanco;

Acessar o banco de dados
> use nomedoBanco;

Criar tabela no banco de dados
> create table nomedaTabela(
>  nome VARCHAR(50),
>  email VARCHAR(100),
>  idade INT
> );

Ver as tabelas que existem dentro do banco
> show tables;

Ver os dados contidos na tabela
> describe nomedaTabela;

#### Inserir dados (INSERT)
> insert into nomedaTabela(nome, email, idade) values (
>  "Pedro Victor da Fonseca",
>  "contatopedrorn@gmail.com",
>  21
>);

#### Listar os dados da tabela (SELECT)
> select * from nomedaTabela;

Listar um dado específico (SELECT)
> select nome from nomedaTabela;

Listar dados usando where (WHERE)
> select * from nomedaTabela where idade = 21;

#### Deletar dados (DELETE)
> delete from nomedaTabela where idade = 45;

Apagar tabela
> delete from nomedaTabela;

#### Atualizar algum dado na tabela (UPDATE)
> update nomedaTabela set email = "contatopedro@ufrn.edu.br" where email = "contatopedro@ufrn.edu";