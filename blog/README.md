# Banco de dados relacionais

O relacionamento é quando criamos depedências entre os dados. É fazer uma tabela conversar com a outra.

As músicas de uma banda são um tipo de relacionameto, pois as músicas pertencem a essa banda.

### Tipos de relacionamento

> **1 para 1** : Atividade --Pertence-> Aula 
> **1 para N** : Vendendor --Atende-> Cliente1, Cliente2, Cliente3
 > **N para N** : Produto1, Produto2, Produto3 ya Nota1, Nota2, Nota3

### Relacionamento entre o banco de dados 
Para fazermos o relacionamento 1 para 1: **belongsTo**

``` Article.belongsTo(Categoria, { constraints: false });  ```
Ou seja, um artigo pertence a uma categoria.

Para relacionamento 1 para muitos: **hasMany**

```Categoria.hasMany(Article, { constraints: false });```
Uma categoria tem muitos artigos

### Qual utilidade dos relacionamentos?
Em um conceito fundamental, usaremos para performace

## Gerando um slug a partir de uma string
> yarn add slugify

```
  Categorie.create({
      title: title,
      slug: slugify(title),
    })
```

## Métodos Interessantes
```isNaN ``` Verificar se variável é um número

```findByPk``` Verificar id no banco de dados (mais rápido)

## Adicionando TinyMCE 

[Link para download](https://www.tiny.cloud/get-tiny/self-hosted/)

### Instalação

Adicionando o Tiny no projeto:
``` <script src="/js/tinymce/tinymce.min.js"></script> ```

Configurando
``` 
<script>
  tinymce.init({
    selector: 'textarea',
    language: 'pt_BR',
    plugins: [ 'advlist autolink link image lists print preview hr searchreplace wordcount fullscren insertdatetime media save table paste emoticons' ],
    tinycomments_author: 'Pedro V',
  });
</script>
```

## EJS
Para exibir conteúdo dinâmico
``` <%-article.body %>  ```

## Hash da Senha 
> yarn add bcryptjs