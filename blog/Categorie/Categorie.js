const Sequelize = require('sequelize');
const connection = require('../database/database');

const Categorie = connection.define('categorie', {
  title: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  slug: {
    type: Sequelize.STRING,
    allowNull: false,
  },
});
/* Categorie.sync({ force: true })
  .then(() => console.log('Tabela Categoria criada com sucesso'))
  .catch((err) => {
    console.log(`Erro Categoria: ${err}`);
  }); */

module.exports = Categorie;
