const express = require('express');
const Router = express.Router();
const Categorie = require('./Categorie');
const slugify = require('slugify');
const AdminAuth = require('../middlewares/adminAuth');

Router.get('/categories', AdminAuth, (req, res) => {
  res.send('Olá');
});

// Cadastro de nova categoria
Router.get('/admin/categories/new', AdminAuth, (req, res) => {
  res.render('admin/Categories/new');
});

Router.post('/categories/save', (req, res) => {
  const title = req.body.title;
  if (title !== undefined)
    Categorie.create({
      title: title,
      slug: slugify(title, { lower: true, strict: true }),
    }).then(() => {
      res.redirect('/admin/categories');
      console.log(`Categoria criada:\nTítulo: ${title}`);
    });
  else res.redirect('/admin/categories');
});

Router.get('/admin/categories', AdminAuth, (req, res) => {
  Categorie.findAll().then((categories) => {
    res.render('admin/Categories/index', { categories: categories });
  });
});

Router.post('/categories/delete', (req, res) => {
  const id = req.body.id;
  if (id !== undefined) {
    if (!isNaN(id)) {
      Categorie.destroy({
        where: {
          id: id,
        },
      }).then(() => {
        console.log('Categoria deleteda com sucesso');
        res.redirect('/admin/categories');
      });
    } else res.redirect('/admin/categories'); // Vefiricar se a variável id não for um número
  } else res.redirect('/admin/categories');
});

Router.get('/admin/categories/edit/:id', AdminAuth, (req, res) => {
  const id = req.params.id;

  if (isNaN(id)) {
    res.redirect('/admin/categories');
  }

  Categorie.findByPk(id)
    .then((categorie) => {
      if (categorie !== undefined) {
        res.render('admin/Categories/edit', { categorie: categorie });
      } else {
        res.redirect('/admin/categories');
      }
    })
    .catch((err) => {
      res.redirect('/admin/categories');
    });
});

Router.post('/categories/update', (req, res) => {
  const { id, title } = req.body;

  Categorie.update(
    { title: title, slug: slugify(title, { lower: true, strict: true }) },
    {
      where: {
        id: id,
      },
    }
  ).then(() => {
    res.redirect('/admin/categories');
  });
});

module.exports = Router;
