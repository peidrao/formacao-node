const Sequelize = require('sequelize');
const connection = require('../database/database');

const Admin = connection.define('admin', {
  email: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  password: {
    type: Sequelize.STRING,
    allowNull: false,
  },
});
/* Admin.sync({ force: true })
  .then(() => console.log('Tabela Categoria criada com sucesso'))
  .catch((err) => {
    console.log(`Erro Categoria: ${err}`);
  });
 */
module.exports = Admin;
