const Admin = require('./Admin');
const express = require('express');
const Router = express.Router();
const bcrypt = require('bcryptjs');

const AdminAuth = require('../middlewares/adminAuth');

Router.get('/admin/users', AdminAuth, (req, res) => {
  Admin.findAll().then((admins) => {
    res.render('admin/Admin/admins', { admins: admins });
  });
});

Router.get('/admin/users/create', AdminAuth, (req, res) => {
  res.render('admin/Admin/create');
});

Router.post('/users/create', (req, res) => {
  const { email, password } = req.body;

  Admin.findOne({ where: { email: email } }).then((admin) => {
    if (admin == undefined) {
      const salt = bcrypt.genSaltSync(10);
      const hash = bcrypt.hashSync(password, salt);

      Admin.create({
        email: email,
        password: hash,
      })
        .then(() => {
          res.redirect('/');
        })
        .catch((err) => {
          res.redirect('/');
          console.log(`Erro: ${err}`);
        });
    } else {
      console.log('email já exste');
      res.redirect('/admin/users/create');
    }
  });
});

Router.get('/login', (req, res) => {
  res.render('admin/Admin/login');
});

Router.post('/authenticate', (req, res) => {
  const { email, password } = req.body;
  Admin.findOne({ where: { email: email } })
    .then((admin) => {
      if (admin !== undefined) {
        // Validar senha
        const correct = bcrypt.compareSync(password, admin.password);
        if (correct) {
          req.session.admin = {
            id: admin.id,
            email: admin.email,
          };
          res.redirect('/admin/articles');
        } else {
          res.redirect('/login');
        }
      } else {
        res.redirect('/login');
      }
    })
    .catch((err) => {
      res.redirect('/login');
      console.log(`erro: ${err}`);
    });
});

Router.get('/logout', (req, res) => {
  req.session.user = undefined;
  res.redirect('/');
});

module.exports = Router;
