const { Sequelize } = require('sequelize');

const connection = new Sequelize('blogdoPedro', 'root', 'tecnologiamariadb', {
  host: 'localhost',
  dialect: 'mysql',
  logging: false,
  timezone: '-03:00',
});

module.exports = connection;
