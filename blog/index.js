const express = require('express');
const session = require('express-session');
const app = express();
const bodyParser = require('body-parser');
const path = require('path');

const connection = require('./database/database');

const CategoriesController = require('./Categorie/categorieController');
const ArticlesController = require('./Articles/articlesController');
const AdminController = require('./Admin/adminController');

const Categorie = require('./Categorie/Categorie');
const Article = require('./Articles/Article');
const Admin = require('./Admin/Admin');

app.set('view engine', 'ejs');
/* app.set('view engine', __dirname + '/views'); */
/* app.set('view engine', path.join(__dirname, '')); */
app.use(express.static(path.join(__dirname, 'public')));
/* app.use(express.static('public')); */

/* Sessões */

app.use(
  session({
    secret: 'ljaskdckl9euq983laljdlczxcççwwqwed@@@sa',
    cookie: {
      maxAge: 10000000000000000,
    },
  })
);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

connection
  .authenticate()
  .then(() => {
    console.log('Banco criado');
  })
  .catch((err) => {
    console.log(`Erro: ${err}`);
  });

app.use('/', CategoriesController);
app.use('/', ArticlesController);
app.use('/', AdminController);

app.get('/', (req, res) => {
  Article.findAll({ order: [['id', 'DESC']], limit: 4 }).then((articles) => {
    Categorie.findAll().then((categories) => {
      res.render('index', { articles: articles, categories: categories });
    });
  });
});

app.get('/:slug', (req, res) => {
  const slug = req.params.slug;

  Article.findOne({
    where: {
      slug: slug,
    },
  })
    .then((article) => {
      if (article !== undefined) {
        Categorie.findAll().then((categories) => {
          res.render('article', { article: article, categories: categories });
        });
      } else res.redirect('/');
    })
    .catch((err) => {
      res.redirect('/');
      console.log(`Erro -> ${err}`);
    });
});

app.get('/categorie/:slug', (req, res) => {
  const slug = req.params.slug;
  Categorie.findOne({
    where: { slug: slug },
    include: [{ model: Article }],
  })
    .then((categorie) => {
      if (categorie != undefined) {
        Categorie.findAll().then((categories) => {
          res.render('categorie', {
            articles: categorie.articles,
            categories: categories,
          });
        });
      } else res.redirect('/');
    })
    .catch((err) => {
      res.redirect('/');
      console.log(`Erro: ${err}`);
    });
});

const PORT = 8080;
app.listen(PORT, () => {
  console.log(`Servidor rodado na porta: ${PORT}`);
});
