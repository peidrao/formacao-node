const express = require('express');
const Router = express.Router();
const Categorie = require('../Categorie/Categorie');
const Article = require('./Article');
const slugify = require('slugify');
const AdminAuth = require('../middlewares/adminAuth');

Router.get('/admin/articles', AdminAuth, (req, res) => {
  Article.findAll({
    include: [
      {
        model: Categorie,
      },
    ],
  }).then((articles) => {
    res.render('admin/Articles/index', { articles: articles });
  });
});

Router.get('/admin/articles/new', AdminAuth, (req, res) => {
  Categorie.findAll().then((categories) => {
    res.render('admin/Articles/new', { categories: categories });
  });
});

Router.post('/articles/save', (req, res) => {
  const { title, body, categorie } = req.body;

  Article.create({
    title: title,
    slug: slugify(title, { lower: true, strict: true }),
    body: body,
    categorieId: categorie,
  }).then(() => {
    res.redirect('/admin/articles');
    console.log('artigo criado');
  });
});

Router.post('/articles/delete', (req, res) => {
  const id = req.body.id;
  if (id !== undefined) {
    if (!isNaN(id)) {
      Article.destroy({
        where: {
          id: id,
        },
      }).then(() => {
        console.log('Artigo deleteda com sucesso');
        res.redirect('/admin/articles');
      });
    } else res.redirect('/admin/articles'); // Vefiricar se a variável id não for um número
  } else res.redirect('/admin/articles');
});

Router.get('/admin/articles/edit/:id', AdminAuth, (req, res) => {
  const id = req.params.id;

  if (isNaN(id)) {
    res.redirect('/admin/articles');
  }

  Article.findByPk(id)
    .then((article) => {
      if (article !== undefined) {
        Categorie.findAll().then((categories) => {
          res.render('admin/Articles/edit', {
            article: article,
            categories: categories,
          });
        });
      } else {
        res.redirect('/');
      }
    })
    .catch((err) => {
      res.redirect('/');
    });
});

Router.post('/articles/update', (req, res) => {
  const { id, title, categorie } = req.body;
  const body = req.body.body;

  Article.update(
    {
      title: title,
      body: body,
      categorieId: categorie,
      slug: slugify(title, { lower: true, strict: true }),
    },
    {
      where: {
        id: id,
      },
    }
  )
    .then(() => {
      res.redirect('/admin/articles');
    })
    .catch((err) => {
      res.redirect('/');
      console.log(`Erro: ${err}`);
    });
});

Router.get('/articles/page/:num', AdminAuth, (req, res) => {
  let page = req.params.num;
  let offset = 0;

  if (isNaN(page) || page == 1) {
    offset = 0;
  } else {
    offset = (parseInt(page) - 1) * 4;
  }

  Article.findAndCountAll({
    limit: 4,
    offset: offset,
    order: [['id', 'DESC']],
  }).then((articles) => {
    let next;
    if (offset + 4 >= articles.count) {
      next = false;
    } else {
      next = true;
    }

    let result = {
      page: parseInt(page),
      next: next,
      articles: articles,
    };

    Categorie.findAll().then((categories) => {
      res.render('admin/Articles/page', {
        result: result,
        categories: categories,
      });
    });
  });
});

module.exports = Router;
