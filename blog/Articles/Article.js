const Sequelize = require('sequelize');
const connection = require('../database/database');
const Categoria = require('../Categorie/Categorie');

const Article = connection.define('article', {
  title: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  slug: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  body: {
    type: Sequelize.TEXT,
    allowNull: false,
  },
});

Article.belongsTo(Categoria, { constraints: false });
Categoria.hasMany(Article, { constraints: false });

/* Article.sync({ force: true })
  .then(() => console.log('Tabela Article criada com sucesso'))
  .catch((err) => {
    console.log(`Erro Article: ${err}`);
  }); */

module.exports = Article;
